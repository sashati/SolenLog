# SolenLog [![Docker Pulls](https://img.shields.io/docker/pulls/sdshariati/solenlog.svg)]()
To run SoleNode instances on a machine, we use docker-compose and Dockers. The docker-compose script will run a MongoDB instance beside SoleNode and connect them together. 
## Installation
For the base step we need to install docker and docker-compose
### Install Docker-CE
#### Debian 
Please follow these steps: https://docs.docker.com/engine/installation/linux/docker-ce/debian/#install-using-the-repository

### Install docker-compose
#### Debian 
```
sudo curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```
## Get project and docker images
```
git clone https://github.com/saeedsh/SolenLog.git
cd SolenLog
docker-compose pull
```
## Run services
`docker-compose up -d`

Now the service is accesible via http://localhost:9000 and you can try to login with user/pass: admin/admin

## Update service
`docker-compose pull && docker-compose up -d`

# Troubleshoots
Q: **How to reset admin password?**

A: Just remove `users` collection in MondoDB and restart the service. Then you can login to system with user/pass: admin/admin
